/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jv21701.bondarenko.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author vovan
 */
public class StringUtil {

    public static boolean isEmpty(String value) {
        if (value == null) {
            return true;
        } else {
            return value.isEmpty();
        }
    }

    public static String findAndReplace(String source, String regex, String replacenent) {
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(source);
        return m.replaceAll(replacenent);
    }

}
